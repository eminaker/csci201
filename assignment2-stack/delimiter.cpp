/*
 * Assignment 2 - Stack - delimiter.cpp
 * CSCI201
 * Eric Minaker
 *
 * The following will check a user-supplied string of delimiters, whitespace
 * characters, and alpha-numeric characters, checking for empty sets of
 * delimiters and mismatched or unbalanced delimiters. The program will ignore
 * whitespace entirely and treat strings such as ([]) such that the parens 
 * would no be empty but the square brackets would be flagged empty.
 *
 * An error will output and all processing will cease on mismatched delimiters
 * as well as unbalanced delimiters (a higher number of right hand delimiters
 * than left had delimiters).
 *
 * A warning will be issued on blank sets of delimiters but processing will
 * continue.
 *
 * The program can be ended with a single 'x' or 'X'.
 *
 * Compilation Instructions:
 * g++ -o compiled delimiter.cpp ItemType.cpp stack.cpp
 *
 * Operation:
 * Run from the compiled directory with ./compiled
 * Input strings to be checked, one at a time
 * Exit with 'x' or 'X'
 */
#include <iostream>
#include <string>
#include "stack.h"

using namespace std;

int main(){
    string input;
    ItemType currItem, tempItem;
    Stack stackAttack;
    int tempToken;
    char legibleCurr;
    char legibleTemp;
    bool foundError=false;

    do{
        /*
         * Loop asks for user input until given, decodes and checks input
         * once supplied, and after checking and providing feedback asks for
         * another line of input. This will continue until the user enters
         * 'x' or 'X'.
         */
        stackAttack.~Stack();
        cout << "Please enter the string to be checked" << endl;
        getline(cin, input);
        foundError=false;

        for(int i=0; (i<input.size()) && !foundError; i++){
            if(input[i] != ' ' && input[i] != '\t'){
                tempToken = currItem.decode(input[i]);
                /*
                 * Token decode key:
                 * 0 = LPAREN
                 * 1 = LCURLY
                 * 2 = LSQUARE
                 * 3 = RPAREN
                 * 4 = RCURLY
                 * 5 = RSQUARE
                 * 6 = OTHER
                 */

                // OTHER
                if(tempToken == 6){
                    if(!stackAttack.isEmpty()){
                        tempItem = stackAttack.pop();
                        tempItem.setBlank(false);
                        stackAttack.push(tempItem);
                    }
                }

                // LXX Cases
                else if(tempToken == 0 ||
                        tempToken == 1 ||
                        tempToken == 2){
                    if(!stackAttack.isEmpty()){
                        tempItem = stackAttack.pop();
                        tempItem.setBlank(false);
                        stackAttack.push(tempItem);
                        cout << "Pushing" << endl;
                        stackAttack.push(currItem);
                        cout << "Just pushed, using getHead: " << endl;
                        cout << stackAttack.getHead() << endl;
                    }
                    else if(stackAttack.isEmpty()){
                        cout << "Pushing" << endl;
                        stackAttack.push(currItem);
                        cout << "Just pushed, using getHead: " << endl;
                        cout << stackAttack.getHead() << endl;
                    }
                }

                // RXX Cases
                else if(tempToken == 3 ||
                        tempToken == 4 ||
                        tempToken == 5){
                    if(!stackAttack.isEmpty()){
                        cout << "Printing Stack:" << endl;
                        stackAttack.printStack();
                        cout << "Depth: " << stackAttack.getDepth() << endl;
                        currItem = stackAttack.pop();
                        cout << "Just popped: " << endl;
                        cout << currItem << endl;

                        // Processing for user-friendly output
                        switch(currItem.getToken()){
                            case 0:
                                legibleCurr = '(';
                                break;
                            case 1:
                                legibleCurr = '{';
                                break;
                            case 2:
                                legibleCurr = '[';
                                break;
                            default:
                                legibleCurr = 'E';
                        }
                         switch(tempToken){
                            case 3:
                                legibleTemp = ')';
                                break;
                            case 4:
                                legibleTemp = '}';
                                break;
                            case 5:
                                legibleTemp = ']';
                                break;
                            default:
                                legibleTemp = 'E';
                        }

                        // Warning and Error Output
                        if(currItem.getToken() != (int)(tempToken - 3)){
                            cout << "ERROR: Delimiters " << legibleCurr <<
                                " and " << legibleTemp <<
                                " do not match." << endl;
                            stackAttack.~Stack();
                            foundError = true;
                        }
                        else if(currItem.getBlank()){
                            cout << "WARNING: Delimiters, " << legibleCurr <<
                                " and " << legibleTemp <<
                                " do not contain data." << endl;
                        }
                    }// End of if(!stackAttack.isEmpty())
                    else{
                        cout << "ERROR: Too many right hand delimiters."
                            << endl;
                        foundError = true;
                    }

                    currItem.setBlank(true);
                }// End of RH token check
            }// End of WS check
        }// End of for loop
    }while(!(input.size()==1 && (input[0]=='x'||input[0]=='X')));
}
