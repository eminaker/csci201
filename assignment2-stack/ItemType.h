/*
 * Assignment 2 - Stack - ItemType.h
 * CSCI201
 * Eric Minaker
 *
 * Definition for ItemType.cpp, itended for implementation with stack.cpp
 * and delimiter.cpp.
 */
#include <iostream>

using namespace std;

#ifndef ITEMTYPE
#define ITEMTYPE

enum SYMBOL{
    LPAREN,
    LCURLY,
    LSQUARE,
    RPAREN,
    RCURLY,
    RSQUARE,
    OTHER};

class ItemType
{
    public:
	ItemType();		// Constructor
	~ItemType();		// Destructor
	ItemType(const ItemType & origElement);		// Copy constructor
	const ItemType & operator=(const ItemType &rhs); // Assignment op overload
    void setBlank(bool); // isBlank mutator
    bool getBlank(); // isBlank accessor
    SYMBOL getToken(); // token accessor
    int decode(char); // token mutator
	// Stream operator overloads...
	friend ostream& operator<< (ostream& out, const ItemType &it);
	friend ifstream& operator>> (ifstream& in, ItemType &it);

    private:
	// Our data
    bool isBlank;
    SYMBOL token;
};

#endif

