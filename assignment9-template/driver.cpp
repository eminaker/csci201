#include <iostream>
#include "queue.h"
#include "ItemType.h"
#include "string.h"

// g++ -o driver driver.cpp ItemType.cpp

using namespace std;

// Prototype for a generic function to decode the action to be taken
template <typename SType>
void takeAction(Queue<SType> &myQueue, char comm, SType data);

int main(int argc, char *argv[])
{
    char command, datatype, dummy;
    ifstream infile;

    // What types of queues do we need?
    Queue<int> myIntQueue;
    int intData;
    Queue<char> myCharQueue;
    char charData;
    Queue<long> myLongQueue;
    long longData;
    Queue<float> myFloatQueue;
    float floatData;
    Queue<double> myDoubleQueue;
    double doubleData;
    Queue<string> myStringQueue;
    string stringData;
    Queue<ItemType> myItemQueue;
    ItemType dictEntry;
    string inWord;
    string inDef;

    // Did we get an input filename on the command-line?
    if (argc != 2)
    {
	cout << "ERROR: Wrong number of parameters\n";
	cout << "Usage: " << argv[0] << " <input filename>\n";
	return(-1);
    }

    // Can we open the input filename?
    infile.open(argv[1]);
    if (infile.fail())
    {
	cout << "ERROR: Unable to open input file " << argv[1] << endl;
	return(-1);
    }

    infile >> datatype;  // Odd read to get infile.eof() to work correctly
    while (!infile.eof())
    {
	infile >> command;
    	switch (datatype)
    	{
    	    case 'i':
    		// Get the integer data
    		infile >> intData;
    		// Call the generic queue action function
    		takeAction(myIntQueue, command, intData);
    	    break;

    	    case 'l':  // That's a lowercase L!
    		infile >> longData;
            takeAction(myLongQueue, command, longData);
    	    break;

    	    case 'f':
    		infile >> floatData;
            takeAction(myFloatQueue, command, floatData);
    	    break;

    	    case 'd':
    		infile >> doubleData;
            takeAction(myDoubleQueue, command, doubleData);
    	    break;

    	    case 'c':
    		// Get the character data
    		infile >> charData;
    		// Call the generic queue action function
    		takeAction(myCharQueue, command, charData);
    	    break;

    	    case 'w':
    		infile >> inWord;
    	    infile >> dummy;
    		getline(infile, inDef);
    		dictEntry.setWord(inWord);
    		dictEntry.setDefinition(inDef);
            takeAction(myItemQueue, command, dictEntry);
    	    break;

    	    case 's':
    		infile >> dummy;
            getline(infile, stringData);
            takeAction(myStringQueue, command, stringData);
    	    break;

    	    default:
    		cout << "ERROR: Unknown type identifier: " << datatype << endl;
    	}
    	infile >> datatype;
    }
    // Clean up the file stream after we're done reading
    infile.close();

    return 0;
}

template <typename SType>
void takeAction(Queue<SType> &myQueue, char comm, SType data)
{
    SType myData;
    string comment;

    switch (comm)
    {
	case 'N':
	    if (myQueue.isEmpty())
        	cout << "Empty" << endl;
	    else
		      cout << "Not Empty" << endl;
        break;

        case 'L':
            cout << "The depth of the queue is " << myQueue.getDepth() << endl;
        break;

        case 'F':
            try{
                myData = myQueue.getFront();
                cout << "The top of the queue is " << myData << endl;
            }
            catch(string exceptParm){
                cout << "There's an error" << endl;
                cout << exceptParm << endl;
            }
        break;

        case 'D':
            try{
                myData = myQueue.dequeue();
                cout << "The data just dequeued is " << myData << endl;
            }
            catch(string exceptParm){
                cout << "There's an error" << endl;
                cout << exceptParm << endl;
            }
        break;

        case 'P':
            myQueue.printQueue();
        break;

        case 'X':
            myQueue.~Queue<SType>();
        break;

        case 'E':
            myQueue.enqueue(data);
        break;

        default:
            cout << "ERROR: Bad action " << comm <<" read from input file\n";
        break;
    }
}
