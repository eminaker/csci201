/*
 * Eric Minaker
 * CSCI201
 * Fall 2015
 * Assignment 9 - Templated Queue
 *
 * The following program is an implementation of a templated queue that has
 * been tested with floats, doubles, ints, strings, key:value dictionary pairs,
 * longs, and chars. It includes methods for checking an empty queue,
 * enqueueing, dequeueing, getting the front of the queue, printing the
 * depth/length of the queue, as well as a copy constructor and assignment
 * overload.
 *
 * Dequeue and getFront will throw string exceptions on an empty queue.
 *
 * Compilation string:
 * g++ -o Minaker driver.cpp ItemType.cpp
*/

#include <iostream>
#include "string.h"

using namespace std;

#ifndef QUEUE
#define QUEUE

template <typename SType>
class Queue
{
    public:
    	Queue();
    	~Queue();
    	Queue(const Queue & origElement);
    	const Queue & operator=(const Queue &rhs);
    	bool isEmpty();
    	void enqueue(SType data);
    	SType dequeue();
        SType getFront();      // Retrieve the data at the top of the queue
        void printQueue();      // Prettyprint the contents of the queue
        int getDepth();         // Retrieve the # of items in the queue

    private:
        struct QueueNode
        {
            SType item;
            QueueNode *next;
        };

        QueueNode *top, *back;
        int depth;
};
// Constructor
template <typename SType>
Queue<SType>::Queue()
{
    top = back = NULL;
    depth = 0;
}
// Destructor
template <typename SType>
Queue<SType>::~Queue()
{
    while (!isEmpty())
	   dequeue();
}
// Copy Constructor
template <typename SType>
Queue<SType>::Queue(const Queue & origElement)
{
    QueueNode *tempOrig, *tempThis;

    tempOrig = origElement.top;

    if (origElement.top != NULL)
    {
    	top = new(QueueNode);
    	top->item = tempOrig->item;
    	top->next = NULL;
    	tempOrig = tempOrig->next;
    	tempThis = top;
    	while (tempOrig != NULL)
    	{
    	    tempThis->next = new(QueueNode);
    	    tempThis = tempThis->next;
    	    tempThis->item = tempOrig->item;
    	    tempThis->next = NULL;
    	    tempOrig = tempOrig->next;
    	}
    }
    else
    	top = NULL;

}
// Assignment Overload
template <typename SType>
const Queue<SType> & Queue<SType>::operator=(const Queue &rhs)
{
    QueueNode *tempRhs, tempThis;

    tempRhs = rhs.top;


    if (rhs.top != NULL)
    {
    	top = new(QueueNode);
    	top->item = tempRhs->item;
    	top->next = NULL;
    	tempRhs = tempRhs->next;
    	tempThis = top;
    	while (tempRhs != NULL)
    	{
    	    tempThis->next = new(QueueNode);
    	    tempThis = tempThis->next;
    	    tempThis->item = tempRhs->item;
    	    tempThis->next = NULL;
    	    tempRhs = tempRhs->next;
    	}
    }
    else
    	top = NULL;

}

template <typename SType>
bool Queue<SType>::isEmpty()
{
   return(top == NULL);
}

template <typename SType>
void Queue<SType>::enqueue(SType data)
{
    QueueNode *newNode;

    newNode = new QueueNode;
    newNode->item = data;
    newNode->next = NULL;

    if(isEmpty()){
        top = newNode;
        back = newNode;
        depth++;
    }
    else{
        back->next = newNode;
        back = newNode;
        depth++;
    }
}

template <typename SType>
SType Queue<SType>::dequeue()
{
    SType result;
    QueueNode *temp;

    if(isEmpty()){
        throw string("ERROR: Queue is empty");
    }
    else{
        temp = top;
        result = top->item;
        top = top->next;

        if(top == NULL){
            back = NULL;
        }

        temp->next = NULL;
        delete temp;
        depth--;
    }

    return(result);
}

template <typename SType>
SType Queue<SType>::getFront()
{
    SType retVal;

    if(isEmpty()){
        throw string("ERROR: Queue is empty.");
    }
    else
    {
    	retVal = top->item;
    }

    return(retVal);
}

template <typename SType>
void Queue<SType>::printQueue()
{
    QueueNode *temp;

    cout << "The Queue contents are:\n";
    temp = top;
    if (!isEmpty())
    {
    	while (temp != NULL)
    	{
    	    cout << "\t" << temp->item << endl;
    	    temp = temp->next;
    	}
    }
    else
    	cout << "\tNothing\n";
}

template <typename SType>
int Queue<SType>::getDepth()
{
    return(depth);
}
#endif
