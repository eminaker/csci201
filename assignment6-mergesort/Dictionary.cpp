/*
 * Eric Minaker
 * CSCI201
 * Fall 2015
 * Assignment 5 - Merge Sort
 *
 * Reads a file, dictionary.txt, and places the words and definitions into an
 * array as key:value pairs. It then prints the array in the order it was
 * read. The array is then passed to merge sort, sorted, and output as a
 * sorted array.
 *
 * Compilation String:
 * g++ -o Minaker Dictionary.cpp ItemType.cpp Mergesort.cpp
 *
 * Opeartion Instructions:
 * Just run the compiled program, there's no user interaction.
 */
#include <iostream>
#include "ItemType.h"
#include "Mergesort.h"

using namespace std;

int main(){
    int MaxWords = 50;
    FILE *inFile;
    char inWord[WordSize];
    char inDef[DefinitionSize];
    ItemType dictionary[MaxWords];
    int nextSlot = 0, i;
    MergeSort Sort;


    /* Try to open the input file c-style... */
    inFile = fopen("dictionary.txt","r");

    /* Check if the file opened successfully */
    if (inFile == NULL){
        /* Output the error message */
        cout << "ERROR: Unable to open the input file dictionary.txt\n";
        cout << "Check your current working directory and PATH\n";

        /* Bail out with a generic error return value */
        return -1;
    }

    /* As long as we're able to read a word from the input file */
    while (fscanf(inFile, "%s\n", inWord) > 0){
        /* Try to read the definition...
         * if we fail, print the error and bailout */
        if (fgets(inDef, DefinitionSize, inFile) == NULL){
            cout << "INPUT ERROR: Word " << inWord;
            cout << " does not have a definition\n";
            return -1;
        }
        /*****************************************************
         * Since fgets also reads the newline character '\n' *
         * from the infile's line, we need to replace this   *
         * '\n' with the end-of-string character '\0'.	     *
         *****************************************************/
        inDef[strlen(inDef) - 1] = '\0';

        // If we get to here, we have the word and definition
        // Throw it into the ItemType
        dictionary[nextSlot].setWord(inWord);
        dictionary[nextSlot].setDefinition(inDef);


        // Increment the index for the next word
        nextSlot++;
    }
    cout << "Unsorted Dictionary: " << endl;
    cout << endl;
    // output unsorted dictionary
    for(int j = 0; j <=(nextSlot-1); j++)
        cout << dictionary[j] << endl;

    cout << endl;
    cout << "Sorting... " << endl;
    cout << endl;
    // Sort!
    Sort.mergeSort(dictionary,0,(nextSlot-1));

    cout << "Sorted Dictionary: " << endl;
    cout << endl;
    // Output again
    for(int k=0;k<=(nextSlot-1);k++)
        cout << dictionary[k]<<endl;
    
    return 0;
}
