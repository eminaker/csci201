/*
 * Mergesort.cpp
 * Itended for implementation with Dictionary.cpp and ItemType.cpp
 * Implements a MergeSort on any data type provided the operators are
 * overloaded.
 *
 * MergeSort will take a set of data and split it into two equal-sized pieces
 * and recursively call mergeSort on each half. When the size of a piece of
 * data reaches 1 or 0, that piece is returned. When both left and
 * right mergeSort calls return, merge will be called which compares and sorts
 * the values.
 */
#include "ItemType.h"
#include "Mergesort.h"

MergeSort::MergeSort()
{
}

MergeSort::~MergeSort()
{
}

void MergeSort::mergeSort(ItemType myArray[], int start, int end)
{
    // If start == end, only one element, return
    // else, set middle to start+end/2
    // sort left values
    // sort right values, don't resort mid
    // merge values
    int mid;                                // init mid

    if(start == end)                        // If one element, stop recursion
        return;

    else{                                   // else sort
        mid = (start+end) / 2;              //split in half
        mergeSort(myArray, start, mid);     //sort left
        mergeSort(myArray, mid+1, end);     //sort right
        merge(myArray, start, mid, end);    //merge
    }
}

void MergeSort::merge(ItemType myArray[], int start, int mid, int end)
{
    ItemType buffer[end-start+1];
    int buffIndex = 0, left = start, right = mid + 1;

    while (left <= mid && right <= end)
    {
	if (myArray[left] < myArray[right])
	{
	    buffer[buffIndex] = myArray[left];
	    buffIndex++;
	    left++;
	}
	else
	{
	    buffer[buffIndex] = myArray[right];
	    buffIndex++;
	    right++;
	}
    }
    while (left <= mid)
    {
	buffer[buffIndex] = myArray[left];
	buffIndex++;
	left++;
    }
    while (right <= end)
    {
	buffer[buffIndex] = myArray[right];
	buffIndex++;
	right++;
    }

    buffIndex = 0;
    for (left = start; left <= end; left++)
    {
	myArray[left] = buffer[buffIndex];
	buffIndex++;
    }

    return;
}

