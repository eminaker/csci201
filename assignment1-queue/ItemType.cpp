#include <iostream>
#include <fstream>

using namespace std;

#include "ItemType.h"

ItemType::ItemType()
	: data_byte('\0')
{
}

ItemType::~ItemType()
{
}

ItemType::ItemType(const ItemType & origElement)
{
	data_byte = origElement.data_byte;
}

const ItemType & ItemType::operator=(const ItemType &rhs)
{
	if (this != &rhs)
	{
		data_byte = rhs.data_byte;
	}

	return *this;
}

unsigned char ItemType::getData()
{
	return(data_byte);
}

void ItemType::setData(unsigned char data)
{
	data_byte = data;
}

ostream& operator<<(ostream& out, const ItemType &it)
{
    out << it.data_byte;

    return out;
}

ifstream& operator>>(ifstream& in, ItemType &it)
{
    unsigned char dummy;

    in >> dummy;
    it.data_byte = dummy;

    return in;
}

