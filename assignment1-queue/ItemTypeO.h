#include <iostream>

using namespace std;

#ifndef ITEMTYPE
#define ITEMTYPE

class ItemType
{
    public:
        ItemType();             // Constructor
        ~ItemType();            // Destructor
        ItemType(const ItemType & origElement);         // Copy constructor
        const ItemType & operator=(const ItemType &rhs); // Assignment op overload
        void setData(unsigned char data);       // Mutator
        unsigned char getData();                // Accessor

        // Stream operator overloads...
        friend ostream& operator<< (ostream& out, const ItemType &it);
        friend ifstream& operator>> (ifstream& in, ItemType &it);

    private:
        // Our data
        char data_byte;
};

#endif

