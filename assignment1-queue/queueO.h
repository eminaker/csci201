/*
 * Eric Minaker
 * CSCI201 Fall 2015
 * Assignment 1 - Dynamic Queues
 *
 * This is the header file for the queue class implemented in queue.cpp. A
 * description of the class and its functions can be found in queue.cpp at
 * the beginning of the file and a detailed breakdown of each function is
 * given at the start of the function.
 */
#ifndef QUEUE
#define QUEUE

#include "ItemType.h"

class Queue
{
    public:
        Queue();                    // Constructor
        ~Queue();                   // Destructor
        bool isEmpty();             // Checks for nodes in queue
        void enqueue(ItemType);     // Adds a node to the queue
        ItemType dequeue();         // Deletes and returns the head item
        ItemType getFront();        // Returns the head, non-destructive
        void printQueue();          // Prints entire queue, non-destructive
        int getLength();            // Returns length of the queue
    private:
        struct QueueNode
        {
            ItemType item;
            QueueNode *next;
        };
        int length;
        QueueNode *front, *back;
};
#endif

