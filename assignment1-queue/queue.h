#ifndef QUEUE
#define QUEUE

#include "ItemType.h"

class Queue
{
    public:
	Queue();		// Constructor
	~Queue();		// Destructor
	bool isEmpty();		// Empty queue test
	void enqueue(ItemType data);	// Insert data into the queue
	ItemType dequeue();	// Delete data from the queue
	ItemType getFront();	// Retrieve the data at the front of the queue
	void printQueue();	// Prettyprint the contents of the queue
	int getLength();	// Retrieve the # of items in the queue

    private:
	struct QueueNode
	{
	    ItemType item;
	    QueueNode *next;
	};

	QueueNode *front, *back;
	int length;
};
#endif
