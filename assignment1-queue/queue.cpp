/*
 * Eric Minaker
 * CSCI201 Fall 2015
 * Assignment 1 - Dynamic Queues
 * 
 * This class will implement a queue that allows the user to enqueue data to 
 * the front, dequeue from the back, print the entire queue with one ItemType
 * per line, check if the queue is empty, print the front data of the queue
 * without removing it, and check the length of the queue. All of these
 * funtions are implemented with the ItemType class, which is a single char.
 * 
 * Compilation string:
 * g++ -o compiled driver.cpp ItemType.cpp queue.cpp
 */
#include "ItemType.h"
#include "queue.h"

Queue::Queue()
{
    /*
     * 1. Init front and back pointers
     * 2. Init length var
     */
    front = back = NULL;
    length = 0;
}

Queue::~Queue()
{
    /*
     * 1. Iterate through entire list
     * 2. Safely delete each node
     * 3. Reset front, back, and length
     */
    QueueNode *nodePtr, *nextNode;

    nodePtr = front;

    while(nodePtr != NULL){
        nextNode = nodePtr->next;

        nodePtr->next = NULL;
        delete nodePtr;

        nodePtr = nextNode;
    }

    length = 0;
    front = back = NULL;
}

bool Queue::isEmpty()
{
    /*
     * 1. Check if the first position has data
     * 2. Let user know if list contains nodes
     */
    return(front == NULL);
}

void Queue::enqueue(ItemType data)
{
    /*
     * 1. Create a new QueueNode
     * 2. Add the enqueued data to the item var of the node
     * 3. Set new node next to NULL
     * 4. If list is empty
     *     4.1. Set front to new node
     *     4.2. Set back to new node
     *     4.3. Increment length
     * 5. If list contain data
     *     5.1. Set next of current back to new node
     *     5.2. Set back to new node
     *     5.3. Increment length
     */
    QueueNode *newNode;

    newNode = new QueueNode;
    newNode->item = data;
    newNode->next = NULL;

    if(isEmpty()){
        front = newNode;
        back = newNode;
        length++;
    }
    else{
        back->next = newNode;
        back = newNode;
        length++;
    }
}

ItemType Queue::dequeue()
{
    /*
     * 1. If list is empty do nothing
     * 2. If list contains data
     *     2.1. Make temp node pointer to hold next of current front
     *     2.2. Make ItemType to hold data of current front
     *     2.3. Set temp pointer to front
     *     2.4. Set new ItemType to front item.
     *     2.5. Set front to front next
     *     2.6. Check if last node is deleted, if so, NULL back
     *     2.7. Delete temp
     *     2.8. Decrement length
     *     2.9. Return previous item of front stored in new ItemType
     */
    ItemType result;
    QueueNode *temp;

    if(!isEmpty()){
        temp = front;
        result = front->item;
        front = front->next;
        
        if(front == NULL){
            back = NULL;
        }
        
        temp->next = NULL;
        delete temp;
        length--;
    }

    return(result);
}

ItemType Queue::getFront()
{
    /*
     * 1. If list is empty do nothing
     * 2. If list contains data
     *     2.1. Create ItemType to hold front item
     *     2.2. Set new ItemType to front item
     *     2.3. Return new ItemType var
     */
    ItemType result;
    
    if(!isEmpty()){
        result = front->item;
    }

    return(result);
}

void Queue::printQueue()
{
    /*
     * 1. If list is empty do nothing
     * 2. If list contains data
     *     2.1. Create new node pointers to iterate through list
     *     2.2. Set one of the pointers to front
     *     2.3. In a while loop, walk through list and cout item
     */
    if(!isEmpty()){
        QueueNode *nodePtr, *nextNode;

        nodePtr = front;

        while(nodePtr != NULL){
            nextNode = nodePtr->next;
            cout << nodePtr->item << endl;
            nodePtr = nextNode;
        }
    }
}


int Queue::getLength()
{
    /*
     * 1. Print length
     */
    return(length);
}

