/*
 * Implementation of a binary search tree.
 * Includes insertion, printing in-order, pre-order, and post-order.
 * Finding the maximum and minimum values.
 * Finding one specific value.
 * Removal is stubbed currently and will be completed later.
 */
#include <iostream>
#include "ItemType.h"
#include "BST.h"

BTree::BTree()
{
    root = NULL;
}

BTree::~BTree()
{
    // Done, not tested
    if (!isEmpty())
	destructHelper(root);
    root = NULL;
}

void BTree::insert(ItemType data)
{
    // Is the tree is empty?
    //     Allocate a new node into root
    //     Copy insertion data into the new node
    //     Make sure the pointers (left and right) are NULL
    // Else
    //     Call the insert helper with the root of the tree
    //         and the insertion data
    if(root==NULL){
        root = new BTreeNode;
        root->appData = data;
        root->left = NULL;
        root->right = NULL;
    }
    else{
        insertHelper(root, data);
    }
    return;
}

void BTree::remove(ItemType data)
{
    // CODE GOES HERE
    // JUST NOT NOW
    return;
}

ItemType BTree::find(ItemType data)
{
    // If the tree is empty, error and return blank ItemType.
    // Else call findHelper on root and recurse.
    ItemType match;
    ItemType dummy; 
    if(!isEmpty()){
        cout << "Searching the Binary Search Tree..." << endl;
        match = findHelper(root, data);
    }
    else{
        cout << "ERROR SEARCHING: Tree is empty." << endl;
        match = dummy;
    }
    return match;
}

void BTree::printInOrder()
{
    cout << "The Binary Search Tree contains (inorder):\n";
    printInOrderHelper(root);
    cout << endl;
}

// Preorder public frontend
void BTree::printPreOrder()
{
    cout << "The Binary Search Tree contains (preorder):\n";
    printPreOrderHelper(root);
    cout << endl;
}

void BTree::printPostOrder()
{
    cout << "The Binary Search Tree contains (postorder):\n";
    printPostOrderHelper(root);
    cout << endl;
}


bool BTree::isEmpty()
{
    return(root == NULL);
}

ItemType BTree::maxBTree()
{
    ItemType dummy;

    if (!isEmpty())
    {
	return maxBTreeHelper(root);
    }
    else
    {
	cout <<"\t"<< "ERROR: In maxBTree there is no tree (nor spoon)\n";
	return dummy;
    }
}

ItemType BTree::minBTree()
{
    ItemType dummy;

    if (!isEmpty())
    {
	return minBTreeHelper(root);
    }
    else
    {
	cout <<"\t"<< "ERROR: In minBTree there is no tree (nor spoon)\n";
	return dummy;
    }
}

void BTree::destructHelper(BTreeNode *current)
{
    // Done, not tested
    // Walk through tree removing all values and NULL'ing pointers.
    ItemType dummy;

    if (current->left != NULL)
    {
	destructHelper(current->left);
	current->left = NULL;
    }
    if (current->right != NULL)
    {
	destructHelper(current->right);
	current->right = NULL;
    }
    current->appData = dummy;		// Clear out sensitive data

    delete(current);

    return;
}

void BTree::insertHelper(BTreeNode *current, ItemType data)
{
    // If the insert data equals the data in current, bail
    //    out with an error
    // Is our insert data less than the data in current?
    //    Is the left of current NULL?
    //        Allocate a new node into current's left
    //        Copy data into new node
    //        NULL the left and right pointers of the new node
    //    Else recurse to the left
    // We must go right...
    //    Can we go right (i.e. right is NULL)?
    //        Allocate a new node into current's right
    //        Copy data into new node
    //        NULL the left and right pointers of the new node

    if(data == current->appData){
        cout <<"\t"<< "ERROR: Data already in tree." << endl;
    }
    if(data < current->appData){
        if(current->left == NULL){
           BTreeNode *newNode = new BTreeNode;
           newNode->appData = data;
           newNode->left = NULL;
           newNode->right = NULL;
           current->left = newNode;
        }
        else{
            insertHelper(current->left, data);
        }
    }
    if(data > current->appData){
        if(current->right == NULL){
            BTreeNode *newNode = new BTreeNode;
            newNode->appData = data;
            newNode->left = NULL;
            newNode->right = NULL;
            current->right = newNode;
        }
        else{
            insertHelper(current->right, data);
        }
    }
    return;
}

void BTree::removeHelper(BTreeNode *current, ItemType data)
{
    // CODE GOES HERE
    // JUST NOT NOW
    return;
}

ItemType BTree::findHelper(BTreeNode *current, ItemType data)
{
    // Check if current is equal to data being searched for.
    // If not, check if data is less than or greater than current.
    // If less, check if left is NULL. If so, it's at the left-most point and
    // no matching data is present.
    // If greater, check if right is NULL. If so, it's at the right-most point
    // and no matching data is present.
    // If branch is not NULL, recurse that direction.
    ItemType dummy;
    if(current->appData == data){
        return current->appData;
    }
    else if(data < current->appData){
        if(current->left == NULL){
            cout <<"\t"<< "ERROR: Data not found." << endl;
            return dummy;
        }
        else{
            return(findHelper(current->left, data));
        }
    }
    else if(data > current->appData){
        if(current->right == NULL){
            cout <<"\t"<< "ERROR: Data not found." << endl;
            return dummy;
        }
        else{
            return(findHelper(current->right, data));
        }
    }
}

void BTree::printInOrderHelper(BTreeNode *current)
{
    // Print the tree sorted, least to greatest.
    if (current != NULL)
    {
	printInOrderHelper(current->left);
	cout << "\t" << current->appData << endl;
	printInOrderHelper(current->right);
    }

    return;
}

void BTree::printPreOrderHelper(BTreeNode *current){
    // Print the tree in order of nodes
    if(current != NULL){
        cout << "\t" << current->appData << endl;
        printPreOrderHelper(current->left);
        printPreOrderHelper(current->right);
    }
    return;
}

void BTree::printPostOrderHelper(BTreeNode *current){
    // Print the left branch, then the right branch.
    if(current != NULL){
        printPostOrderHelper(current->left);
        printPostOrderHelper(current->right);
        cout << "\t" << current->appData << endl;
    }
    return;
}

ItemType BTree::maxBTreeHelper(BTreeNode *current)
{
    // Walk all of the way right on the tree, printing the last value.
    ItemType foundItem;
    BTreeNode *temp;
    temp = root;
    if(isEmpty()){
        cout <<"\t"<< "ERROR: Tree is empty." << endl;
    }
    else{
        while(temp->right != NULL)
            temp = temp->right;
        foundItem = temp->appData;
    }

    return foundItem;
}

ItemType BTree::minBTreeHelper(BTreeNode *current)
{
    // Walk all of the way left on the tree, printing the last value.
    ItemType foundItem;
    BTreeNode *temp;
    temp = root;
    if(isEmpty()){
        cout <<"\t"<< "ERROR: Tree is empty." << endl;
    }
    else{
        while(temp->left != NULL)
            temp = temp->left;
        foundItem = temp->appData;
    }

    return foundItem;
}
