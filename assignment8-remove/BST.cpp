/*
 * Eric Minaker
 * CSCI201
 * Fall 2015
 * Assignment 8 - Binary Search Tree Remove
 *
 * Reads a file, dictionary.txt, and places the words and definitions into an
 * array as key:value pairs. It then transfers this array to a binary search
 * tree, in the order it was read in from the file. It then prints the tree
 * in-order, pre-order, and post-order. It then prints the maximum and minimum
 * values from the tree. It then finds an arbitrary dictionary entry before
 * allowing the user to input other words to find. If the word is not in the
 * dictionary, it ignores the returned value. The user can quit with q.
 *
 * This assignment adds remove to the tree, remove specific operations are
 * detailed within the removeHelper function.
 *
 * Compilation String:
 * g++ -o Minaker Dictionary.cpp ItemType.cpp BTree.cpp
 *
 * Opeartion Instructions:
 * Run the program, follow the on-screen prompts.
 */
#include <iostream>
#include "ItemType.h"
#include "BST.h"

BTree::BTree()
{
    root = NULL;
}

BTree::~BTree()
{
    if (!isEmpty())
	    destructHelper(root);
    root = NULL;
}

void BTree::insert(ItemType data)
{
    // Is the tree is empty?
    //     Allocate a new node into root
    //     Copy insertion data into the new node
    //     Make sure the pointers (left and right) are NULL
    // Else
    //     Call the insert helper with the root of the tree
    //         and the insertion data
    if(root==NULL){
        root = new BTreeNode;
        root->appData = data;
        root->left = NULL;
        root->right = NULL;
    }
    else{
        insertHelper(root, data);
    }
    return;
}

void BTree::remove(ItemType data)
{
    if(root != NULL)
        removeHelper(&root, root, data);
    else
        cout << "ERROR: Tree is empty" << endl;
    return;
}

ItemType BTree::find(ItemType data)
{
    // If the tree is empty, error and return blank ItemType.
    // Else call findHelper on root and recurse.
    ItemType match;
    ItemType dummy; 
    if(!isEmpty()){
        cout << "Searching the Binary Search Tree..." << endl;
        match = findHelper(root, data);
    }
    else{
        cout << "ERROR SEARCHING: Tree is empty." << endl;
        match = dummy;
    }
    return match;
}

void BTree::printInOrder()
{
    cout << "The Binary Search Tree contains (inorder):\n";
    printInOrderHelper(root);
    cout << endl;
}

// Preorder public frontend
void BTree::printPreOrder()
{
    cout << "The Binary Search Tree contains (preorder):\n";
    printPreOrderHelper(root);
    cout << endl;
}

void BTree::printPostOrder()
{
    cout << "The Binary Search Tree contains (postorder):\n";
    printPostOrderHelper(root);
    cout << endl;
}


bool BTree::isEmpty()
{
    return(root == NULL);
}

ItemType BTree::maxBTree()
{
    ItemType dummy;

    if (!isEmpty())
    {
	    return maxBTreeHelper(root);
    }
    else
    {
	    cout <<"\t"<< "ERROR: In maxBTree there is no tree (nor spoon)\n";
	    return dummy;
    }
}

ItemType BTree::minBTree()
{
    ItemType dummy;

    if (!isEmpty())
    {
	    return minBTreeHelper(root);
    }
    else
    {
	    cout <<"\t"<< "ERROR: In minBTree there is no tree (nor spoon)\n";
	    return dummy;
    }
}

void BTree::destructHelper(BTreeNode *current)
{
    // Done, not tested
    // Walk through tree removing all values and NULL'ing pointers.
    ItemType dummy;

    if (current->left != NULL)
    {
        destructHelper(current->left);
        current->left = NULL;
    }
    if (current->right != NULL)
    {
        destructHelper(current->right);
        current->right = NULL;
    }
    current->appData = dummy;		// Clear out sensitive data

    delete(current);

    return;
}

void BTree::insertHelper(BTreeNode *current, ItemType data)
{
    // If the insert data equals the data in current, bail
    //    out with an error
    // Is our insert data less than the data in current?
    //    Is the left of current NULL?
    //        Allocate a new node into current's left
    //        Copy data into new node
    //        NULL the left and right pointers of the new node
    //    Else recurse to the left
    // We must go right...
    //    Can we go right (i.e. right is NULL)?
    //        Allocate a new node into current's right
    //        Copy data into new node
    //        NULL the left and right pointers of the new node

    if(data == current->appData){
        cout <<"\t"<< "ERROR: Data already in tree." << endl;
    }
    if(data < current->appData){
        if(current->left == NULL){
           BTreeNode *newNode = new BTreeNode;
           newNode->appData = data;
           newNode->left = NULL;
           newNode->right = NULL;
           current->left = newNode;
        }
        else{
            insertHelper(current->left, data);
        }
    }
    if(data > current->appData){
        if(current->right == NULL){
            BTreeNode *newNode = new BTreeNode;
            newNode->appData = data;
            newNode->left = NULL;
            newNode->right = NULL;
            current->right = newNode;
        }
        else{
            insertHelper(current->right, data);
        }
    }
    return;
}

void BTree::removeHelper(BTreeNode **parent, BTreeNode *current, ItemType data)
{
    // Did we find it?
    if((*parent)->appData == data){
        // Is it a leaf?
        if((*parent)->left == NULL && (*parent)->right==NULL){
            // NULL node, free memory.
            (*parent) = NULL;
            delete(*parent);
        }
        // Does it have only one child?
        else if((*parent)->left == NULL || (*parent)->right ==NULL){
            // Find out whether the node to delete has a child on their left
            // or their right.
            // Set the node to delete equal to the child.
            if((*parent)->left != NULL){
                (*parent) = (*parent)->left;
            }
            else{
                (*parent) = (*parent)->right;
            }
        }
        // Two children
        else if((*parent)->left != NULL && (*parent)->right != NULL){
            // Find biggest value of left subtree - inorder predecessor
            // Copy appData from temp pointer to current
            // Recursively remove inorder predecessor
            BTreeNode *temp;
            temp = (*parent)->left;
            while(temp->right != NULL){
                temp = temp->right;
            }
            current->appData = temp->appData;
            removeHelper(&(current->left), current->left, current->appData);
        }
    }
    // Data not found? Keep searching
    else if(data < (*parent)->appData){
        if((*parent)->left!=NULL)
            removeHelper(&((*parent)->left), (*parent)->left, data);
        else
            cout << "ERROR: Value not found" << endl;
    }
    else{
        if((*parent)->right!=NULL)
            removeHelper(&((*parent)->right), (*parent)->right, data);
        else
            cout << "ERROR: Value not found" << endl;
    }

    return;
}

ItemType BTree::findHelper(BTreeNode *current, ItemType data)
{
    // Check if NULL is returned, value is not in tree, return word with no
    // definition.
    // If data searched for is less than data in node passed, go left.
    // If data is greater, go right.
    // Else current node is equal to data, set match to appData.
    ItemType match;
    if(current == NULL){
        match.setWord(data.getWord());
    }
    else if(data < current->appData){
        match = findHelper(current->left, data);
    }
    else if(data > current->appData){
        match = findHelper(current->right, data);
    }
    else{
        match = current->appData;
    }
    return match;
}

void BTree::printInOrderHelper(BTreeNode *current)
{
    // Print the tree sorted, least to greatest.
    if (current != NULL)
    {
        printInOrderHelper(current->left);
        cout << "\t" << current->appData << endl;
        printInOrderHelper(current->right);
    }

    return;
}

void BTree::printPreOrderHelper(BTreeNode *current){
    // Print the tree in order of nodes
    if(current != NULL){
        cout << "\t" << current->appData << endl;
        printPreOrderHelper(current->left);
        printPreOrderHelper(current->right);
    }
    return;
}

void BTree::printPostOrderHelper(BTreeNode *current){
    // Print the left branch, then the right branch.
    if(current != NULL){
        printPostOrderHelper(current->left);
        printPostOrderHelper(current->right);
        cout << "\t" << current->appData << endl;
    }
    return;
}

ItemType BTree::maxBTreeHelper(BTreeNode *current)
{
    // Walk all of the way right on the tree, printing the last value.
    ItemType foundItem;
    BTreeNode *temp;
    temp = root;
    if(isEmpty()){
        cout <<"\t"<< "ERROR: Tree is empty." << endl;
    }
    else{
        while(temp->right != NULL)
            temp = temp->right;
        foundItem = temp->appData;
    }

    return foundItem;
}

ItemType BTree::minBTreeHelper(BTreeNode *current)
{
    // Walk all of the way left on the tree, printing the last value.
    ItemType foundItem;
    BTreeNode *temp;
    temp = root;
    if(isEmpty()){
        cout <<"\t"<< "ERROR: Tree is empty." << endl;
    }
    else{
        while(temp->left != NULL)
            temp = temp->left;
        foundItem = temp->appData;
    }

    return foundItem;
}
