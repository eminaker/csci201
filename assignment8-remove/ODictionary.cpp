/*
 * Eric Minaker
 * CSCI201
 * Fall 2015
 * Assignment 6/3 - Binary Search Tree
 *
 * Reads a file, dictionary.txt, and places the words and definitions into an
 * array as key:value pairs. It then transfers this array to a binary search
 * tree, in the order it was read in from the file. It then prints the tree
 * in-order, pre-order, and post-order. It then prints the maximum and minimum
 * values from the tree. It then finds an arbitrary dictionary entry before
 * allowing the user to input other words to find. If the word is not in the
 * dictionary, it ignores the returned value. The user can quit with q.
 *
 * Compilation String:
 * g++ -o Minaker Dictionary.cpp ItemType.cpp BTree.cpp
 *
 * Opeartion Instructions:
 * Run the program, follow the on-screen prompts.
 */
#include <iostream>
#include "ItemType.h"
#include "BTree.h"

using namespace std;

int main(){
    int MaxWords = 50;
    FILE *inFile;
    char inWord[WordSize];
    char inDef[DefinitionSize];
    ItemType dictionary[MaxWords];
    int nextSlot = 0, i;
    BTree tree;
    ItemType temp;
    char searchWord[WordSize];
    ItemType result;

    /* Try to open the input file c-style... */
    inFile = fopen("dictionary.txt","r");

    /* Check if the file opened successfully */
    if (inFile == NULL){
        /* Output the error message */
        cout << "ERROR: Unable to open the input file dictionary.txt\n";
        cout << "Check your current working directory and PATH\n";

        /* Bail out with a generic error return value */
        return -1;
    }

    /* As long as we're able to read a word from the input file */
    while (fscanf(inFile, "%s\n", inWord) > 0){
        /* Try to read the definition...
         * if we fail, print the error and bailout */
        if (fgets(inDef, DefinitionSize, inFile) == NULL){
            cout << "INPUT ERROR: Word " << inWord;
            cout << " does not have a definition\n";
            return -1;
        }
        /*****************************************************
         * Since fgets also reads the newline character '\n' *
         * from the infile's line, we need to replace this   *
         * '\n' with the end-of-string character '\0'.	     *
         *****************************************************/
        inDef[strlen(inDef) - 1] = '\0';

        // If we get to here, we have the word and definition
        // Throw it into the ItemType
        dictionary[nextSlot].setWord(inWord);
        dictionary[nextSlot].setDefinition(inDef);


        // Increment the index for the next word
        nextSlot++;
    }
    // Inserting all values into the tree 
    for(int i=0;i<=nextSlot-1;i++){
        tree.insert(dictionary[i]);
    }
    // Print
    tree.printInOrder();
    tree.printPreOrder();
    tree.printPostOrder();

    cout << "Printing maximum..." << endl;
    cout <<"\t"<< tree.maxBTree() << endl;
    cout << "Printing minimum..." << endl;
    cout << "\t"<<tree.minBTree() << endl;
    // Static find
    cout << "Finding \"Handle\"..." << endl;
    cout << "\t"<<tree.find(dictionary[5]) << endl;
    // User find
    do{
        cout << "Enter another word to search for, enter q to quit: ";
        cin.getline(searchWord,WordSize);
        temp.setWord(searchWord);
        if(searchWord[0]!='q')
            result = tree.find(temp);
        if(result.getWord()[0]!='\0')
            cout <<"\t"<<result<<endl;
    }while(searchWord[0]!='q');
    

    return 0;
}
