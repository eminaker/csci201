#include <iostream>
#include <fstream>
#include <string.h>
#include "ItemType.h"

ItemType::ItemType()		// Constructor
{
    priority = 0;
    procName = "";
}

ItemType::~ItemType()		// Destructor
{
}

ItemType::ItemType(const ItemType & origElement) // Copy constructor
{
    priority = origElement.priority;
    procName = origElement.procName;
}

const ItemType & ItemType::operator=(const ItemType &rhs)
{
    if (this != &rhs)
    {
	priority = rhs.priority;
	procName = rhs.procName;
    }

    return rhs;
}

bool operator<(const ItemType &lhs, const ItemType &rhs)
{
    return(lhs.priority < rhs.priority);
}

bool operator>(const ItemType &lhs, const ItemType &rhs)
{
    return(rhs < lhs);
}

bool operator<=(const ItemType &lhs, const ItemType &rhs)
{
    return(!(rhs < lhs));
}

bool operator>=(const ItemType &lhs, const ItemType &rhs)
{
    return(!(lhs < rhs));
}

bool operator==(const ItemType &lhs, const ItemType &rhs)
{
    return(!(lhs < rhs) && !(rhs < lhs));
}

bool operator!=(const ItemType &lhs, const ItemType &rhs)
{
    return((lhs < rhs) || (rhs < lhs));
}

void ItemType::setPriority(double prior)         // Mutator
{
    string errStr = "ERROR: Invalid priority (negative) in ItemType::setPriority.";

    if (prior >= 0)
	priority = prior;
    else
	throw(errStr);
}

double ItemType::getPriority()              // Accessor
{
    return priority;
}

void ItemType::setProcessName(string pName)      // Mutator
{
    string errStr = "ERROR: Invalid process name (blank) in ItemType::setProcessName.";
    if (pName != "")
	procName = pName;
    else
	throw(errStr);
}

string ItemType::getProcessName()                // Accessor
{
    return procName;
}

ostream& operator<< (ostream& out, const ItemType &it)
{
    out << it.procName << " " << it.priority;

    return out;
}

istream& operator>> (istream& inPut, ItemType &it)
{
    string inName;
    double inPrior;

    inPut >> inName;
    if (inPut.fail())
	return inPut;
    inPut >> inPrior;
    it.setProcessName(inName);
    it.setPriority(inPrior);

    return inPut;
}
