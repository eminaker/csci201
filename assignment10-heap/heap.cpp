#include <iostream>
#include <cstdlib>
#include "heap.h"

using namespace std;

Heap::Heap(int hSize, bool maxHeap)
{
    string badSize = "ERROR: Heap constructor called with invalid heap size";

    if (hSize > 0)
	heapArray = (ItemType *) calloc(hSize+1, sizeof(ItemType));
    else
	throw(badSize);
    heapSize = hSize;
    isMax = maxHeap;
    nextLeaf = 1;
}

Heap::~Heap()
{
}

Heap::Heap(const Heap & origElement)
{
}

const Heap & Heap::operator=(const Heap &rhs)
{
    return rhs;
}

void heapSort(ItemType origArray[], int size_n)
{
    string badSize = "ERROR: The size of the array passed to heapSort is incompatable with the size of the Heap object";
    string badHeapType = "ERROR: Unable to sort using a minHeap";
    ItemType dummy;
    int i;

    if (size_n >= heapSize)
	throw(badSize);
    else if (!isMax)
	throw(badHeapType);
    else
    {
	for(i = 0; i < size_n; i++)
	    fill(origArray[i]);
	init();
	for (i = 0; i < size_n; i++)
	    dummy = extract();
	for (i = 0; i < size_n; i++)
	    origArray[i] = heapArray[i+1];
    }
}

ItemType Heap::extract()
{
    ItemType extracted;

    return extracted;
}

void Heap::insert(const ItemType newItem)
{
}

void Heap::fill(const ItemType newItem)
{
    string fullHeap = "ERROR: Attemtped to add data to a full heap";

    if (nextLeaf <= heapSize)
    {
	heapArray[nextLeaf] = newItem;
	nextLeaf++;
    }
    else
	throw(fullHeap);
}

void Heap::init()
{
    int postion, recurse, right, left;

    for (position = (nextLeaf-1)/2; position < 0; position--)
    {
	right = (position*2)+1;
	left = position*2;
	if (right < nextLeaf)
    }
}

bool Heap::isFull()
{
    return (nextLeaf == heapSize);
}

bool Heap::isEmpty()
{
    return (nextLeaf == 1);
}

ItemType Heap::extractMin()
{
    ItemType extracted;

    return extracted;
}

ItemType Heap::extractMax()
{
    ItemType extracted;

    return extracted;
}

void Heap::insertMax(const ItemType newItem)
{
}

void Heap::insertMin(const ItemType newItem)
{
}
