#include <iostream>
#include <fstream>
#include "ItemType.h"
#include "heap.h"

using namespace std;

int main(int argc, char *argv[])
{
    ItemType inputItem;
    Heap myMaxHeap(42, true);
    Heap myMinHeap(42, false);

    ifstream infile;

    // Did we get an input filename on the command-line?
    if (argc != 2)
    {
        cout << "ERROR: Wrong number of parameters\n";
        cout << "Usage: " << argv[0] << " <input filename>\n";
        return(-1);
    }

    // Can we open the input filename?
    infile.open(argv[1]);
    if (infile.fail())
    {
        cout << "ERROR: Unable to open input file " << argv[1] << endl;
        return(-1);
    }

    infile >> inputItem;  // Odd read to get infile.eof() to work correctly
    while (!infile.eof())
    {
	// For now, just print it
	// Later, we'll just populate the heap array
	cout << inputItem << endl;
	infile >> inputItem;
    }

    return 0;
}
