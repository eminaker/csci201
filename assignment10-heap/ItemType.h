#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

#ifndef ITEMTYPE
#define ITEMTYPE

class ItemType
{
    public:
        ItemType();             // Constructor
        ~ItemType();            // Destructor
        ItemType(const ItemType & origElement);         // Copy constructor
        const ItemType & operator=(const ItemType &rhs); // Assignment op overload
        friend bool operator<(const ItemType &lhs, const ItemType &rhs);	// < op overload
        friend bool operator>(const ItemType &lhs, const ItemType &rhs);	// > op overload
        friend bool operator<=(const ItemType &lhs, const ItemType &rhs);	// <= op overload
        friend bool operator>=(const ItemType &lhs, const ItemType &rhs);	// >= op overload
        friend bool operator==(const ItemType &lhs, const ItemType &rhs);	// == op overload
        friend bool operator!=(const ItemType &lhs, const ItemType &rhs);	// != op overload
        void setPriority(double prior);         // Mutator
        double getPriority();              // Accessor
        void setProcessName(string pName);      // Mutator
        string getProcessName();                // Accessor

        // Stream operator overloads...
        friend ostream& operator<< (ostream& out, const ItemType &it);
        friend istream& operator>> (istream& in, ItemType &it);

    private:
        // Our data
	double priority;
	string procName;
};

#endif
