#include <iostream>
#include <cstdlib>
#include "ItemType.h"

using namespace std;

#ifndef HEAPCLASS
#define HEAPCLASS

class Heap
{
    public:
	Heap(int heapSize, bool maxHeap);
	~Heap();
	Heap(const Heap & origElement);
	const Heap & operator=(const Heap &rhs);
	void heapSort(ItemType origArray[], int size_n);
	ItemType extract();
	void insert(const ItemType newItem);
	void fill(const ItemType newItem);
	void init();
	bool isFull();
	bool isEmpty();
    private:
	ItemType extractMin();
	ItemType extractMax();
	void insertMax(const ItemType newItem);
	void insertMin(const ItemType newItem);

	bool isMax;
	ItemType * heapArray;
	int nextLeaf;
	int heapSize;
};

#endif
