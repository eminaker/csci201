#include <iostream>

#ifndef BTREE
#define BTREE

class BTree{
    public:
        BTree();
        ~BTree();
        void insert(ItemType data);         // Insert Public fronted
        void remove(ItemType data);         // Remove/Delete Public frontend
        ItemType find(ItemType data);       // Search Public frontend
        void printInOrder();                // InOrder Public frontend
        //void printPreOrder();             // PreOrder Public frontend
        //void printPostOrder();            // PostOrder Public frontend
        bool isEmpty();                     // IsEmpty
        ItemType maxBTree();                // Furthest right item
        ItemType minBTree();                // Furthest left item
        //Assignment
        //Copy constructor

    private:
        struct BTreeNode{
            ItemType appData;
            BTreeNode *left, *right;
        };

        // Helper Functions: All called from the public frontend equivalents
        void insertHelper(BTreeNode *current, ItemType data);
        void removeHelper(BTreeNode *current, ItemType data);
        ItemType findHelper(BTreeNode *current, ItemType data);
        void printInOrderHelper(BTreeNode *current);
        //void printPreOrderHelper(BTreeNode *current);
        //void printPostOrderHelper(BTreeNode *current);
        ItemType maxBTreeHelper(BTreeNode *current);
        ItemType minBTreeHelper(BTreeNode *current);

        BTreeNode *root;
};

#endif
