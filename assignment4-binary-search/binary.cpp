#include <iostream>

using namespace std;

void binarySearch(char[], char, int);

int main(){
 
    char query = ' ';
    char array[] = {'a','b','c','d','e','f','g','h','i','j','k','l','o','p'};
    int size = sizeof(array);

    cout << "Key to search for: ";
    cin >> query;
    binarySearch(array, query, size);
 
}

void binarySearch(char array[], char query, int size){

    int mid, start=0, end;
    bool done = false;
    
    end = size-1;
    mid = (end+start)/2;

    while(start <= end && !done){
        mid = (end+start)/2;
        if(array[mid]==query){
            cout << "Your query is at index " << mid << endl;
            done = true;
        }
        // Mid is less than query, move right
        else if(array[mid] < query){
            start=mid+1;
        }
        // Mid is greater than query, move left
        else{
            end=mid-1;
        }
    }
    if(!done){
        cout << "Your value was not found." << endl;
    }

}
