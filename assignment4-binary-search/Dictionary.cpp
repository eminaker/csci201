/*
 * Eric Minaker
 * CSCI 201
 * Fall 2015
 * Assignment 4 - Dictionary Binary Search
 *
 * Supplied a dictionary.txt file, the program will populate an array of
 * key:value ItemTypes and ask the user to enter a word to search for. Once
 * given a word, it will invoke the binSearch class and run a binary search
 * to find and return the word from the dictionary. If the word is not in the
 * dictionary, the class will return the submitted word and a blank definition.
 * The user can exit the program by submitting a line with "quit".
 *
 * Compilation string:
 * g++ -o minaker Dictionary.cpp ItemType.cpp binSearch.cpp
 *
 * Operation:
 * Enter a word to search for, exit by submitting "quit".
 */

#include <iostream>
#include <string.h>
#include <stdio.h>
#include "ItemType.h"
#include "binSearch.h"

const int MaxWords = 50;

using namespace std;

int main()
{
    FILE *inFile;
    char inWord[WordSize];
    char inDef[DefinitionSize];
    ItemType dictionary[MaxWords];
    int nextSlot = 0, i;
    binSearch binarySearch;
    char inQuery[WordSize];
    ItemType query;

    /* Try to open the input file c-style... */
    inFile = fopen("dictionary.txt","r");

    /* Check if the file opened successfully */
    if (inFile == NULL){
        /* Output the error message */
        cout << "ERROR: Unable to open the input file dictionary.txt\n";
        cout << "Check your current working directory and PATH\n";

        /* Bail out with a generic error return value */
        return -1;
    }

    /* As long as we're able to read a word from the input file */
    while (fscanf(inFile, "%s\n", inWord) > 0){
        /* Try to read the definition...
         * if we fail, print the error and bailout */
        if (fgets(inDef, DefinitionSize, inFile) == NULL){
            cout << "INPUT ERROR: Word " << inWord;
            cout << " does not have a definition\n";
            return -1;
        }
        /*****************************************************
         * Since fgets also reads the newline character '\n' *
         * from the infile's line, we need to replace this   *
         * '\n' with the end-of-string character '\0'.	     *
         *****************************************************/
        inDef[strlen(inDef) - 1] = '\0';

        // If we get to here, we have the word and definition
        // Throw it into the ItemType
        dictionary[nextSlot].setWord(inWord);
        dictionary[nextSlot].setDefinition(inDef);


        // Increment the index for the next word
        nextSlot++;
    }

    /*********************************
     * Test the relational operators *
     *********************************/
    if (dictionary[0] < dictionary[1] && !(dictionary[1] < dictionary[0]))
        cout << "Successful < operator\n";
    else
        cout << "Failure of <\n";
    if (dictionary[1] > dictionary[0] && !(dictionary[0] > dictionary[1]))
        cout << "Successful > operator\n";
    else
        cout << "Failure of >\n";
    if (dictionary[0] <= dictionary[1] && !(dictionary[1] <= dictionary[0]))
	    cout << "Successful <= operator\n";
    else
	    cout << "Failure of <=\n";
    if (dictionary[1] >= dictionary[1] && !(dictionary[0] >= dictionary[1]))
	    cout << "Successful >= operator\n";
    else
	    cout << "Failure of >=\n";
    if (dictionary[0] == dictionary[0] && !(dictionary[0] == dictionary[1]))
	    cout << "Successful == operator\n";
    else
	    cout << "Failure of ==\n";
    if (dictionary[0] != dictionary[1] && !(dictionary[0] != dictionary[0]))
	    cout << "Successful != operator\n";
    else
	    cout << "Failure of !=\n";

    cout << "To exit, input quit." << endl;

    /*
     * Ask user for a word to look up in the dictionary until they enter a
     * "quit", then cease input. If word is found, print definition and word, if word
     * is absent from dictionary, print the searched for word with a blank definition.
     */
    do{
        cout << "What word would you like to search for: ";
        scanf("%s", inQuery);

        query.setWord(inQuery);

        query = binarySearch.search(dictionary, query, nextSlot);

        if(strncmp(inQuery, "quit", WordSize) != 0)
            cout << query << endl;

    }while(strncmp(inQuery, "quit", WordSize) != 0);

    return 0;
}

