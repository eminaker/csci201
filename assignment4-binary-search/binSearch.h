/*
 * Definition for binary search class. Itended for implementation with
 * binSearch.cpp.
 */

#include <iostream>
#include <string.h>
#include "ItemType.h"

using namespace std;

#ifndef BINSEARCH
#define BINSEARCH

class binSearch{
    public:
        binSearch();
        ~binSearch();
        ItemType search(ItemType data[], ItemType, int);

    private:

};

#endif
