#include <iostream>
#include <string.h>
#include "ItemType.h"
#include "binSearch.h"

binSearch::binSearch(){
}

binSearch::~binSearch(){
}

/*
 * Accepts an array of ItemTypes, intended to be a key:value dictionary,
 * a query, intended to be a key:value pair with only the key populated,
 * and an int that indicates the number of elements in the key:value
 * dictionary submitted as the first argument.
 *
 * Will perform a binary search on the supplied dictionary looking for the key
 * supplied in the second argument and will return the full ItemType
 * (key:value pair) found in the dictionary.
 *
 * If the key is not located, an error message is output and the last
 * successful search is returned. No error is returned if the line submitted
 * is blank.
 */
ItemType binSearch::search(ItemType data[], ItemType query, int size){
    int mid, start=0, end;
    bool done = false;
    ItemType notFound;
    char clearDef[]={'\0'};

    end = size - 1;
    mid = (end+start)/2;

    while(start <= end && !done){
        mid = (end + start)/2;

        if(data[mid] == query){
            done = true;
        }
        else if(data[mid] < query){
            start = mid+1;
        }
        else{
            end = mid-1;
        }
    }
    if(done){
        query.setDefinition(data[mid].getDefinition());
    }
    else if(!done){
        if(strncmp(query.getWord(), "quit", WordSize)){
            query.setDefinition(clearDef);
        }
    }
    return(query);
}
