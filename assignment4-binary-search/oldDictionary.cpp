#include "string.h"
#include "stdio.h"
#include <iostream>

using namespace std;

const int wordSize = 21;
const int defSize = 81;

int main(){
    char inWord[wordSize];
    char inDef[defSize];
    FILE *inFile;
    inFile = fopen("dictionary.txt","r");

    if(inFile == NULL){
        cout << "ERROR: File could not be opened" << endl;
        return -1;
    }

    while(!feof(inFile)){
        fscanf(inFile, "%s", inWord);
    }
    for(int i=0;i<=10;i++){
        cout << inWord[i] << endl;
    }
}
