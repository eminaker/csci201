#include <iostream>
#include "BTree.h"
#include "ItemType.h"

BTree::BTree(){
    // This is complete. Untested.
    root = NULL;
}

BTree::~BTree(){
    // This is complete. Untested.
    // Destroy and return to free memory every node.
    // Recursive PostOrder traversal to delete all left nodes, then 
    // right nodes, then node.
    // This thing needs a helper.
    // God that sounds awful.
    if(!isEmpty())
        destructHelper(root);
    root = NULL;
}

void BTree::insert(ItemType data){
    // Code here
    return;
}

void BTree::remove(ItemType data){
    // Code here
    return;
}

ItemType BTree::find(ItemType data){
    ItemType match;
    // Code here
    return match;
}

void BTree::printInOrder(){
    cout << "The Binary Search Tree contains (inorder): " << endl;
    printInOrderHelper(root);
    cout << endl;
}

void BTree::printPreOrder(){
    cout << "The Binary Search Tree contains (preorder): " << endl;
    printPreOrderHelper(root);
    cout << endl;
}
void BTree::printPostOrder(){
    cout << "The Binary Search Tree contains (postorder): " << endl;
    printPostOrderHelper(root);
    cout << endl;
}

bool BTree::isEmpty(){
    // This is complete. Untested.
    return(root==NULL);
}

ItemType BTree::maxBTree(){
    ItemType maxTree;
    return maxTree;
}

ItemType BTree::minBTree(){
    ItemType dummy;
    if(!isEmpty())
        return minTreeHelper(root);
    cout << "ERROR: In minBTree there is no tree" << endl;
    return dummy;
}

//Assignment
//Copy constructor


// Helper Functions: All called from the public frontend equivalents
void BTree::insertHelper(BTreeNode *current, ItemType data){
    return;
}

void BTree::removeHelper(BTreeNode *current, ItemType data){
    return;
}

ItemType BTree::findHelper(BTreeNode *current, ItemType data){
    ItemType match;
    return match;
}

void BTree::printInOrderHelper(BTreeNode *current){
    // This is complete. Untested.
    // InOrder traversal printing as we go
    if(current != NULL){
        printInOrderHelper(current->left);
        cout << "\t" << current->appData << endl;
        printInOrderHelper(current->right);
    }
    return;
}

//void printPreOrderHelper(BTreeNode *current);
//void printPostOrderHelper(BTreeNode *current);

ItemType BTree::maxBTreeHelper(BTreeNode *current){
    ItemType foundItem;
    return foundItem;
}

ItemType BTree::minBTreeHelper(BTreeNode *current){
    ItemType foundItem;
    return foundItem;
}

void BTree::destructHelper(BTreeNode *current){
    // This is complete. Untested.
    ItemType dummy;                 // Assume the constuctor makes it empty

    if(current->left != NULL){ // If there's something to the left, destroy it
        destructHelper(current->left);
        current->left = NULL;       // Unnecessary, clear memory addresses
    }

    if(current->right != NULL){// If there's something to the right, destory it
        destructHelper(current->right);
        current->right = NULL;      // Unnecessary, clear memory addresses
    }

    current->appData = dummy;       // Clear out data using empty variable

    delete(current);                // Free memory of node
}
