/*
 * Assignment 2 - Stack - ItemType.cpp
 * CSCI201
 * Eric Minaker
 *
 * The following is a class containing two pieces of data, an enumerated type
 * to decode and store delimiters and a boolean variable that tracks if a set
 * of delimiters contains data or not.
 *
 * The definition for this class can be found in ItemType.h.
 *
 * This class is intended for implementation with stack.cpp and the compilation
 * instructions contained in delimiter.cpp.
 */
#include <iostream>
#include <fstream>

using namespace std;

#include "ItemType.h"

ItemType::ItemType(){
    token = OTHER;
    isBlank = true;
}

ItemType::~ItemType(){
}

ItemType::ItemType(const ItemType & origElement){
    isBlank = origElement.isBlank;
    token = origElement.token;
}

const ItemType & ItemType::operator=(const ItemType &rhs){
	if (this != &rhs)
	{
		token = rhs.token;
        isBlank = rhs.isBlank;
	}

	return *this;
}

ostream& operator<<(ostream& out, const ItemType &it){
    out << "Token: ";
    switch(it.token){
        case LPAREN:
            out << "Left Paren" << endl;
            break;
        case LCURLY:
            out << "Left Curly Brace" << endl;
            break;
        case LSQUARE:
            out << "Left Square Bracket" << endl;
            break;
        case RPAREN:
            out << "Right Paren" << endl;
            break;
        case RCURLY:
            out << "Right Curly Brace" << endl;
            break;
        case RSQUARE:
            out << "Right Square Bracket" << endl;
            break;
        case OTHER:
            out << "OTHER" << endl;
            break;
        default:
            out << "ERROR: Invalid data for token of enum type Smybol."
                << endl;
    }
    if(it.isBlank)
        cout << "isBlank: True" << endl;
    else
        cout << "isBlank: False" << endl;

    return out;
}

ifstream& operator>>(ifstream& in, ItemType &it){
    return in;
}

void ItemType::setBlank(bool passBlank){
    isBlank = passBlank;
}

bool ItemType::getBlank(){
    return(isBlank);
}

SYMBOL ItemType::getToken(){
    return(token);
}

int ItemType::decode(char input){
    /*
     * Matches input characters with valid delimiters and sets token
     * accordingly.
     */
    switch(input){
        case '(':
            token = LPAREN;
            break;
        case '[':
            token = LSQUARE;
            break;
        case'{':
            token = LCURLY;
            break;
        case ')':
            token = RPAREN;
            break;
        case ']':
            token = RSQUARE;
            break;
        case '}':
            token = RCURLY;
            break;
        default:
            token = OTHER;
            break;
    }
    return(token);
}
