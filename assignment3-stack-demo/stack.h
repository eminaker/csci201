/*
 * Assignment 2 - Stack - stack.h
 * CSCI201
 * Eric Minaker
 *
 * Definition for stack.cpp, intended for implementation with ItemType.cpp and
 * delimiter.cpp.
 */
#ifndef STACK
#define STACK

#include "ItemType.h"

class Stack
{
    public:
	Stack();		// Constructor
	~Stack();		// Destructor
	bool isEmpty();		// Empty stack test
	void push(ItemType data);	// Insert data into the stack
	ItemType pop();	// Delete data from the stack
	ItemType getHead();	// Retrieve the data at the front of the stack
	void printStack();	// Prettyprint the contents of the stack
	int getDepth();	// Retrieve the # of items in the stack

    private:
	struct StackNode
	{
	    ItemType item;
	    StackNode *next;
	};

	StackNode *head, *tail;
	int depth;
};
#endif
