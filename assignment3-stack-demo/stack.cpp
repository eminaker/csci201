/*
 * Assignment 2 - Stack - stack.cpp
 * CSCI201
 * Eric Minaker
 * 
 * Implementation of a stack structure containing methods for push, pop,
 * isEmpty, printing, and getFront.
 */
#include "ItemType.h"
#include "stack.h"

Stack::Stack()
{
    /*
     * 1. Init front and back pointers
     * 2. Init length var
     */
    head = tail = NULL;
    depth = 0;
}

Stack::~Stack()
{
    /*
     * 1. Iterate through entire list
     * 2. Safely delete each node
     * 3. Reset front, back, and length
     */
    StackNode *nodePtr;

    while(head != NULL){
        nodePtr = head;
        head = head->next;
        delete nodePtr;
    }

    depth = 0;
}

bool Stack::isEmpty()
{
    /*
     * 1. Check if the first position has data
     * 2. Let user know if list contains nodes
     */
    return(head == NULL);
}

void Stack::push(ItemType data)
{
    /*
     * 1. Create a new StackNode
     * 2. Add the enqueued data to the item var of the node
     * 3. Set new node item to given data
     * 4. Set new node next to head
     * 5. Set head to new node
     * 6. Increment depth
     */
    StackNode *newNode;

    newNode = new StackNode;
    
    newNode->item = data;
    newNode->next = head;
    head = newNode;
    depth++;
}

ItemType Stack::pop()
{
    /*
     * 1. If list is empty do nothing
     * 2. If list contains data
     *     2.1. Make temp node pointer to hold next of current front
     *     2.2. Make ItemType to hold data of current front
     *     2.3. Set temp pointer to front
     *     2.4. Set new ItemType to front item.
     *     2.5. Set front to front next
     *     2.6. Check if last node is deleted, if so, NULL back
     *     2.7. Delete temp
     *     2.8. Decrement length
     *     2.9. Return previous item of front stored in new ItemType
     */
    ItemType result;
    StackNode *temp;

    if(!isEmpty()){
        result = head->item;
        temp = head;
        head = head->next;
        
        if(head == NULL){
            tail = NULL;
        }
        
        temp->next = NULL;
        delete temp;
        depth--;
    }
    else{
        cout << "ERROR: Stack is empty, nothing to pop." << endl;
    }

    return(result);
}

ItemType Stack::getHead()
{
    /*
     * 1. If list is empty do nothing
     * 2. If list contains data
     *     2.1. Create ItemType to hold front item
     *     2.2. Set new ItemType to front item
     *     2.3. Return new ItemType var
     */
    ItemType result;
    
    if(!isEmpty()){
        result = head->item;
    }

    return(result);
}

void Stack::printStack()
{
    /*
     * 1. If list is empty do nothing
     * 2. If list contains data
     *     2.1. Create new node pointers to iterate through list
     *     2.2. Set one of the pointers to front
     *     2.3. In a while loop, walk through list and cout item
     */
    if(!isEmpty()){
        StackNode *nodePtr, *nextNode;

        nodePtr = head;

        while(nodePtr != NULL){
            nextNode = nodePtr->next;
            cout << nodePtr->item << endl;
            nodePtr = nextNode;
        }
    }
}


int Stack::getDepth()
{
    /*
     * 1. Print depth
     */
    return(depth);
}

