/* 
 *      .\^/.          
 *    . |`|/| .         
 *    |\|\|'|/|         
 * .--'-\`|/-''--.      
 *  \`-._\|./.-'/       
 *   >`-._|/.-'<         
 *  '~|/~~|~~\|~'       
 *        |
 *
 * Eric Minaker
 * CSCI201 - Fall 2015
 * Assignment 4(5) - Euclid's Greatest Common Divisor
 *
 * The follow program will ask for the input of two integers and will then,
 * recursively, calculate the greatest common divisor using Euclid's method.
 * The program will terminate if either number is a 0.
 *
 * Compilation String:
 * g++ -o Minaker Euclid.cpp
 *
 * Operation Instructions:
 * Enter numbers when prompted.
 * Enter 0 as either number to terminate the program.
 */
#include <iostream>

using namespace std;

int euclid(int a, int b, int depth){
    /*
     * Recursive function to calculate the GCP of the given numbers, as well
     * as output the depth, or the number of recursive calls, required for
     * the calculation.
     */
    if(b == 0){         // Stopping condition for the recursion
        cout << "The recursive calculation required a depth of " << depth
            << endl;
        return a;
    }
    else{               // Recursion
        depth++;
        return(euclid(b, (a%b), depth));
    }
}

int main(){
    /*
     * Provides basic instructions to the user and accepts input.
     * Makes initial call to the recursive function.
     */
    int a, b, swap=0, depth=1, gcd;

    cout << "To terminate, enter a 0 for either number." << endl;
    cout << endl;

    do{
        cout << "Enter the first number: ";
        cin >> a;
        cout << "Enter the second number: ";
        cin >> b;
        
        if(a < b){
            swap = a;
            a = b;
            b = swap;
        }

        if(a != 0 && b != 0){
            gcd = euclid(a,b,depth);
            cout << "The GCD of " << a << " and " << b << " is " <<
                gcd << endl;
            cout << endl;
            depth = 1;
        }
    }while(a != 0 && b != 0);
}
